I'll keep it short and simple:

* This design is derived from https://raw.githubusercontent.com/galagaking/espaxa/master/docs/Schematic_ESP_AXA_2020-08-26_21-20-54.png
* I also took some inspiration from https://www.superhouse.tv/product/axa-remote-wifi-interface/
* I have replaced the MT3608 boost converter from the original design with LDO regulators. This means that you can't power this via USB. Instead you'll need a power supply that can output 9V 1000mA (anything up to 24V works for the "main" regulator but the closer to 9, the more efficient the power conversion will be)
* Personally I don't use the BMP180 or other I2C sensors in this case since I already monitor the rooms in my house with different hardware. I've exposes the i2c pins on a header in case you need them.
* Currently there is no way to buy this PCB yet, I have to refine the design and order/test it before I'm comfortable selling this. If the tests are successful I'll provide means of purchasing both the bare PCB's as well as complete kits (probably on Tindie). If I get bored I might even offer pre-assembled kits... We'll see.

Wishlist/ToDo:

* Experiment with a PCB that will fit the battery compartment. Challenges: Need a bare ESP8266 (which means more components on this PCB like a 3V3 regulator for example)
* Smash all component text so that the layout looks nicer
* Verify capacitor and resistor sizes (they look pretty large and I'm sure the board can be a bit more compact than it currently is)
