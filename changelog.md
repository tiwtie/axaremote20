# v0.2 (2021-12-27 15:11:00 CET)

* Revised the design to include an I2C breakout
* Reorganised the components
* Shrink the design down to 2.00 x 2.00 inches
* Removed the mounting hole in one of the corners because it wouldn't fit with the Wemos over there

![](images/v0.2/top.png)
![](images/v0.2/bottom.png)


# v0.1 (2021-12-27 13:00:00 CET)

* Initial design that includes all components except for the I2C header. 
* Dimensions 2.34 x 1.95 inches
* Pretty random mounting holes

![](images/v0.1/top.png)
![](images/v0.1/bottom.png)